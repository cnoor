CC = cc
CFLAGS = -Wall -O2 `pkg-config --cflags pangoft2`
LDFLAGS = `pkg-config --libs pangoft2`

all: cnoor
%.o: %.c config.h
	$(CC) -c $(CFLAGS) $<
cnoor: cnoor.o txtwin.o draw.o quran.o
	$(CC) $(LDFLAGS) -o $@ $^
clean:
	rm -f *.o cnoor
