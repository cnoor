#define NSURA		114
#define NAYA		6236

#define SAJDA_NONE	0
#define SAJDA_RECOM	1
#define SAJDA_OBLIG	2

struct quran {
	long ayas[NAYA + 1];
	int fd;
};

struct quran *quran_alloc(char *path);
void quran_aya(struct quran *quran, char *buf, int len, int aya);
void quran_free(struct quran *quran);

int sura_start(int sura);
int sura_ayas(int sura);
char *sura_name(int sura);
int aya_num(int sura, int aya);
int juz_start(int sura, int aya);
int sajda_kind(int sura, int aya);
