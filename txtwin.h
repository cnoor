void txtwin_init(char *name);
void txtwin_free(void);
void txtwin_loop(void);
void txtwin_line(void);
void txtwin_append(char *s, char *font);
