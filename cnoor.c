/*
 * cnoor - a small framebuffer Quran viewer
 *
 * Copyright (C) 2009-2015 Ali Gholami Rudi <ali at rudi dot ir>
 *
 * This program is released under the Modified BSD license.
 */
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "config.h"
#include "txtwin.h"
#include "quran.h"

#define LENGTH(vars)	(sizeof(vars) / sizeof(vars[0]))

static char *adigs[] = {"٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"};

static char *arabic_number(int n)
{
	static char buf[128];
	int digs[16];
	int ndigs = 0;
	int i;
	while (n) {
		digs[ndigs++] = n % 10;
		n /= 10;
	}
	buf[0] = '\0';
	for (i = 0; i < ndigs; i++)
		strcat(buf, adigs[digs[i]]);
	return buf;
}

static void ins_sura(struct quran *quran, struct quran *trans, int sura)
{
	char buf[16 * 1024];
	int start = sura_start(sura);
	int ayas = sura_ayas(sura);
	int i;
	for (i = 0; i < ayas; i++) {
		int juz = juz_start(sura, i + 1);
		int sajda = sajda_kind(sura, i + 1);
		txtwin_line();
		if (juz) {
			sprintf(buf, " %s\n", arabic_number(juz));
			txtwin_append(buf, FONT_JUZ);
		}
		quran_aya(quran, buf, LENGTH(buf), start + i);
		txtwin_append(buf, FONT_QURAN);
		sprintf(buf, " %s ", arabic_number(i + 1));
		txtwin_append(buf, FONT_NUM);
		if (sajda != SAJDA_NONE) {
			char *s = sajda == SAJDA_RECOM ? "*" : "**";
			txtwin_append(s, FONT_SAJDA);
		}
		if (trans) {
			quran_aya(trans, buf, LENGTH(buf), start + i);
			txtwin_append("\n", NULL);
			txtwin_append(buf, FONT_TRANS);
		}
		txtwin_append("\n\n", NULL);
	}
}

int show(int sura)
{
	char name[128];
	struct quran *quran;
	struct quran *trans = NULL;
	sprintf(name, "%d(%s)", sura, sura_name(sura));
	quran = quran_alloc(QURAN_PATH);
	if (TRANS_PATH)
		trans = quran_alloc(TRANS_PATH);
	txtwin_init(name);
	ins_sura(quran, trans, sura);
	txtwin_loop();
	txtwin_free();
	if (trans)
		quran_free(trans);
	quran_free(quran);
	return 0;
}

int main(int argc, char **argv)
{
	int sura;
	if (argc == 1 || !strcmp(argv[1], "-h")) {
		printf("usage: %s sura_number\n", argv[0]);
		return 0;
	}
	sura = atoi(argv[1]);
	if (sura <= 0 || sura > 114) {
		fprintf(stderr, "cnoor: invalid sura number\n");
		return 1;
	}
	show(sura);
	return 0;
}
