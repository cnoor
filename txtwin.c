#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pty.h>

#include <glib.h>
#include <pango/pango.h>
#include <pango/pangoft2.h>

#include "draw.h"
#include "config.h"

#define MAXTEXT			(1 << 20)
#define MAXLINE			((MAXTEXT) >> 4)
#define DPI			196
#define PAGESTEPS		8
#define CTRLKEY(x)		((x) - 96)

#define CR(a)			(((a) >> 16) & 0xff)
#define CG(a)			(((a) >> 8) & 0xff)
#define CB(a)			((a) & 0xff)
#define COLORMERGE(f, b, c)	((b) + (((f) - (b)) * (c) >> 8u))

static PangoContext *context;
static PangoFontMap *fontmap;
static FT_Bitmap *bitmap;
static char name[128];		/* sura name */
static int pcols = 1;		/* bitmap width */
static int prows = 1;		/* bitmap height */
static int head;
static char text[MAXTEXT];
static char *pos = text;
static int pos_off;
static int count;
static int mark[256];
static int lnpos[MAXLINE];	/* line offsets as reported by txtwin_line() */
static int lnrow[MAXLINE];	/* line row in the layout */
static int nlnpos;

static void draw(int sr)
{
	int r, c;
	fbval_t buf[1 << 14];
	int nr = MIN(prows - sr, fb_rows());
	int nc = MIN(pcols, fb_cols());
	for (r = 0; r < nr; r++) {
		for (c = 0; c < nc; c++) {
			int v = bitmap->buffer[(sr + r) * bitmap->pitch + c];
			int r = COLORMERGE(CR(COLOR_FG), CR(COLOR_BG), v);
			int g = COLORMERGE(CG(COLOR_FG), CG(COLOR_BG), v);
			int b = COLORMERGE(CB(COLOR_FG), CB(COLOR_BG), v);
			buf[c] = FB_VAL(r, g, b);
		}
		fb_set(r, 0, buf, pcols);
	}
}

static FT_Bitmap *create_bitmap(int pcols, int prows)
{
	FT_Bitmap *bitmap = g_slice_new(FT_Bitmap);
	bitmap->width = pcols;
	bitmap->pitch = (bitmap->width + 3) & ~3;
	bitmap->rows = prows;
	bitmap->buffer = g_malloc(bitmap->pitch * bitmap->rows);
	bitmap->num_grays = 256;
	bitmap->pixel_mode = ft_pixel_mode_grays;
	memset(bitmap->buffer, 0, bitmap->pitch * bitmap->rows);
	return bitmap;
}

static void destroy_bitmap(FT_Bitmap *bitmap)
{
	g_free(bitmap->buffer);
	g_slice_free(FT_Bitmap, bitmap);
}

static PangoLayout *make_layout(PangoContext *context, char *text, double size)
{
	static PangoFontDescription *font_description;
	PangoLayout *layout = pango_layout_new(context);
	pango_layout_set_markup(layout, text, -1);
	pango_layout_set_wrap(layout, PANGO_WRAP_WORD_CHAR);
	pango_layout_set_width(layout, fb_cols() << 10);
	pango_layout_set_height(layout, -1);
	pango_layout_set_alignment(layout, PANGO_ALIGN_RIGHT);
	pango_layout_set_font_description(layout, font_description);
	pango_font_description_free(font_description);
	return layout;
}

static void do_output(PangoContext *context, char *text)
{
	PangoLayout *layout;
	PangoRectangle rect;
	int i;
	pango_context_set_language(context, pango_language_get_default());
	pango_context_set_base_dir(context, PANGO_DIRECTION_RTL);
	layout = make_layout(context, text, -1);
	pango_layout_get_pixel_extents(layout, NULL, &rect);
	pango_ft2_render_layout(bitmap, layout, 0, 0);
	pcols = MAX(pcols, MAX(rect.x + rect.width,
			PANGO_PIXELS(pango_layout_get_width(layout))));
	prows = MAX(rect.y + rect.height,
		PANGO_PIXELS(pango_layout_get_height(layout)));
	for (i = 0; i < nlnpos; i++) {
		pango_layout_index_to_pos(layout, lnpos[i], &rect);
		lnrow[i] = PANGO_PIXELS(rect.y);
	}
	g_object_unref(layout);
}

static int readkey(void)
{
	char b;
	if (read(0, &b, 1) <= 0)
		return -1;
	return (unsigned char) b;
}

void txtwin_append(char *s, char *font)
{
	snprintf(pos, MAXTEXT - (pos - text),
		 "<span font=\"%s\">%s</span>", font, s);
	pos = strchr(pos, '\0');
	pos_off += strlen(s);
}

void txtwin_line(void)
{
	lnpos[nlnpos++] = pos_off;
}

static int getcount(int def)
{
	int result = count ? count : def;
	count = 0;
	return result;
}

static void printinfo(void)
{
	printf("\x1b[H");
	printf("CNOOR \t\t\t %d%% \t\t\t %s \x1b[K", head * 100 / prows, name);
	fflush(stdout);
}

static void scroll(int h)
{
	mark['\''] = head;
	head = MAX(0, MIN(h, prows - fb_rows()));
}

static void scroll_rel(int diff)
{
	head = MAX(0, MIN(head + diff, prows - fb_rows()));
}

void txtwin_loop(void)
{
	int step = fb_rows() / PAGESTEPS;
	struct termios oldtermios, termios;
	int c, i;
	tcgetattr(0, &termios);
	oldtermios = termios;
	cfmakeraw(&termios);
	tcsetattr(0, TCSAFLUSH, &termios);
	bitmap = create_bitmap(pcols, prows);
	do_output(context, text);
	destroy_bitmap(bitmap);
	bitmap = create_bitmap(pcols, prows);
	do_output(context, text);
	draw(head);
	while ((c = readkey()) >= 0) {
		switch (c) {
		case 27:
			count = 0;
			break;
		case 'q':
			tcsetattr(0, 0, &oldtermios);
			return;
		default:
			if (isdigit(c))
				count = count * 10 + c - '0';
		}
		switch (c) {
		case 'j':
			scroll_rel(+step * getcount(1));
			break;
		case 'k':
			scroll_rel(-step * getcount(1));
			break;
		case CTRLKEY('f'):
		case ' ':
			scroll_rel(+fb_rows() * getcount(1) - step);
			break;
		case CTRLKEY('b'):
		case 127:
			scroll_rel(-fb_rows() * getcount(1) + step);
			break;
		case CTRLKEY('l'):
			break;
		case 'G':
			i = getcount(0);
			if (i > 0 && i < nlnpos)
				scroll(MIN(lnrow[i - 1], prows - fb_rows()));
			else
				scroll(prows - fb_rows());
			break;
		case '%':
			if (count)
				scroll(prows * getcount(0) / 100);
			break;
		case 'i':
			printinfo();
			continue;
		case 'm':
			mark[readkey()] = head;
			continue;
		case '\'':
			c = readkey();
			if (mark[c] >= 0)
				scroll(mark[c]);
			break;
		default:
			/* no need to redraw */
			continue;
		}
		draw(head);
	}
}

void txtwin_init(char *sura)
{
	setlocale(LC_ALL, "");
	g_set_prgname("cnoor");
	fontmap = pango_ft2_font_map_new();
	pango_ft2_font_map_set_resolution(PANGO_FT2_FONT_MAP(fontmap), DPI, DPI);
	context = pango_font_map_create_context(PANGO_FONT_MAP(fontmap));
	strcpy(name, sura);
	if (fb_init())
		exit(1);
	if (FBM_BPP(fb_mode()) != sizeof(fbval_t)) {
		fprintf(stderr, "cnoor: fbval_t doesn't match fb depth\n");
		exit(1);
	}
	printf("\x1b[2J\x1b[H\x1b[?25l");
	printinfo();
	memset(mark, 0xff, sizeof(mark));
}

void txtwin_free(void)
{
	fb_free();
	destroy_bitmap(bitmap);
	g_object_unref(context);
	g_object_unref(fontmap);
	printf("\x1b[?25h\n");
}
