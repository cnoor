#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "quran.h"

#define ARRAY_SIZE(a)	(sizeof(a) / sizeof((a)[0]))
#define MIN(a, b)	((a) < (b) ? (a) : (b))
#define MAX(a, b)	((a) > (b) ? (a) : (b))

static char abuf[1 << 16];
static int apos = -1;
static int acur;
static int aaya;

static int aya_offset(int fd, int aya)
{
	char *s;
	if (aaya > aya || apos == -1) {
		acur = 0;
		aaya = 0;
		apos = 0;
		lseek(fd, apos, 0);
		read(fd, abuf, sizeof(abuf));
	}
	while (aaya < aya) {
		while (!(s = memchr(abuf + acur, '\n', sizeof(abuf) - acur))) {
			acur = 0;
			apos += sizeof(abuf);
			lseek(fd, apos, 0);
			read(fd, abuf, sizeof(abuf));
		}
		acur = s - abuf + 1;
		aaya++;
	}
	return apos + acur;
}

static void *xmalloc(size_t size)
{
	void *value = malloc(size);
	if (!value) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	return value;
}

static void *xerror(char *errmsg)
{
	perror(errmsg);
	exit(1);
}

struct quran *quran_alloc(char *path)
{
	struct quran *quran = xmalloc(sizeof(struct quran));
	int i;
	memset(quran, 0, sizeof(*quran));
	quran->fd = open(path, O_RDONLY);
	if (quran->fd == -1)
		xerror(path);
	for (i = 0; i < NAYA + 1; i++)
		quran->ayas[i] = aya_offset(quran->fd, i);
	return quran;
}

void quran_aya(struct quran *quran, char *buf, int len, int aya)
{
	int n;
	if (quran->ayas[aya] + len > quran->ayas[aya + 1])
		len = quran->ayas[aya + 1] - quran->ayas[aya] - 1;
	lseek(quran->fd, quran->ayas[aya], 0);
	n = read(quran->fd, buf, len);
	while (n > 0 && (buf[n - 1] == '\r' || buf[n - 1] == '\n'))
		n--;
	buf[n] = '\0';
}

void quran_free(struct quran *quran)
{
	close(quran->fd);
	free(quran);
}

static struct suras {
	char *name;
	int ayas;
} suras[] = {
	{"Al-Faatiha", 7}, {"Al-Baqara", 286}, {"Aal-i-Imraan", 200},
	{"An-Nisaa", 176}, {"Al-Maaida", 120}, {"Al-An'aam", 165},
	{"Al-A'raaf", 206}, {"Al-Anfaal", 75}, {"At-Tawba", 129},
	{"Yunus", 109}, {"Hud", 123}, {"Yusuf", 111},
	{"Ar-Ra'd", 43}, {"Ibrahim", 52}, {"Al-Hijr", 99},
	{"An-Nahl", 128}, {"Al-Israa", 111}, {"Al-Kahf", 110},
	{"Maryam", 98}, {"Taa-Haa", 135}, {"Al-Anbiyaa", 112},
	{"Al-Hajj", 78}, {"Al-Muminoon", 118}, {"An-Noor", 64},
	{"Al-Furqaan", 77}, {"Ash-Shu'araa", 227}, {"An-Naml", 93},
	{"Al-Qasas", 88}, {"Al-Ankaboot", 69}, {"Ar-Room", 60},
	{"Luqman", 34}, {"As-Sajda", 30}, {"Al-Ahzaab", 73},
	{"Saba", 54}, {"Faatir", 45}, {"Yaseen", 83},
	{"As-Saaffaat", 182}, {"Saad", 88}, {"Az-Zumar", 75},
	{"Al-Ghaafir", 85}, {"Fussilat", 54}, {"Ash-Shura", 53},
	{"Az-Zukhruf", 89}, {"Ad-Dukhaan", 59}, {"Al-Jaathiya", 37},
	{"Al-Ahqaf", 35}, {"Muhammad", 38}, {"Al-Fath", 29},
	{"Al-Hujuraat", 18}, {"Qaaf", 45}, {"Adh-Dhaariyat", 60},
	{"At-Tur", 49}, {"An-Najm", 62}, {"Al-Qamar", 55},
	{"Ar-Rahmaan", 78}, {"Al-Waaqia", 96}, {"Al-Hadid", 29},
	{"Al-Mujaadila", 22}, {"Al-Hashr", 24}, {"Al-Mumtahana", 13},
	{"As-Saff", 14}, {"Al-Jumu'a", 11}, {"Al-Munaafiqoon", 11},
	{"At-Taghaabun", 18}, {"At-Talaaq", 12}, {"At-Tahrim", 12},
	{"Al-Mulk", 30}, {"Al-Qalam", 52}, {"Al-Haaqqa", 52},
	{"Al-Ma'aarij", 44}, {"Nooh", 28}, {"Al-Jinn", 28},
	{"Al-Muzzammil", 20}, {"Al-Muddaththir", 56}, {"Al-Qiyaama", 40},
	{"Al-Insaan", 31}, {"Al-Mursalaat", 50}, {"An-Naba", 40},
	{"An-Naazi'aat", 46}, {"Abasa", 42}, {"At-Takwir", 29},
	{"Al-Infitaar", 19}, {"Al-Mutaffifin", 36}, {"Al-Inshiqaaq", 25},
	{"Al-Burooj", 22}, {"At-Taariq", 17}, {"Al-A'laa", 19},
	{"Al-Ghaashiya", 26}, {"Al-Fajr", 30}, {"Al-Balad", 20},
	{"Ash-Shams", 15}, {"Al-Lail", 21}, {"Ad-Dhuhaa", 11},
	{"Ash-Sharh", 8}, {"At-Tin", 8}, {"Al-Alaq", 19},
	{"Al-Qadr", 5}, {"Al-Bayyina", 8}, {"Az-Zalzala", 8},
	{"Al-Aadiyaat", 11}, {"Al-Qaari'a", 11}, {"At-Takaathur", 8},
	{"Al-Asr", 3}, {"Al-Humaza", 9}, {"Al-Fil", 5},
	{"Quraish", 4}, {"Al-Maa'un", 7}, {"Al-Kawthar", 3},
	{"Al-Kaafiroon", 6}, {"An-Nasr", 3}, {"Al-Masad", 5},
	{"Al-Ikhlaas", 4}, {"Al-Falaq", 5}, {"An-Naas", 6}
};

int sura_ayas(int sura)
{
	return suras[sura - 1].ayas;
}

char *sura_name(int sura)
{
	return suras[sura - 1].name;
}

int sura_start(int sura)
{
	int i;
	int n = 0;
	for (i = 0; i < sura - 1; i++)
		n += suras[i].ayas;
	return n;
}

int aya_num(int sura, int aya)
{
	return sura_start(sura) + aya;
}

#define AYAHASH(sura, aya)	(((sura) << 10) | (aya))

int juz_start(int sura, int aya)
{
	switch (AYAHASH(sura, aya)) {
	case AYAHASH(1, 1):
		return 1;
	case AYAHASH(2, 142):
		return 2;
	case AYAHASH(2, 253):
		return 3;
	case AYAHASH(3, 93):
		return 4;
	case AYAHASH(4, 24):
		return 5;
	case AYAHASH(4, 148):
		return 6;
	case AYAHASH(5, 82):
		return 7;
	case AYAHASH(6, 111):
		return 8;
	case AYAHASH(7, 88):
		return 9;
	case AYAHASH(8, 41):
		return 10;
	case AYAHASH(9, 93):
		return 11;
	case AYAHASH(11, 6):
		return 12;
	case AYAHASH(12, 53):
		return 13;
	case AYAHASH(15, 1):
		return 14;
	case AYAHASH(17, 1):
		return 15;
	case AYAHASH(18, 75):
		return 16;
	case AYAHASH(21, 1):
		return 17;
	case AYAHASH(23, 1):
		return 18;
	case AYAHASH(25, 21):
		return 19;
	case AYAHASH(27, 56):
		return 20;
	case AYAHASH(29, 46):
		return 21;
	case AYAHASH(33, 31):
		return 22;
	case AYAHASH(36, 28):
		return 23;
	case AYAHASH(39, 32):
		return 24;
	case AYAHASH(41, 47):
		return 25;
	case AYAHASH(46, 1):
		return 26;
	case AYAHASH(51, 31):
		return 27;
	case AYAHASH(58, 1):
		return 28;
	case AYAHASH(67, 1):
		return 29;
	case AYAHASH(78, 1):
		return 30;
	}
	return 0;
}

int sajda_kind(int sura, int aya)
{
	switch (AYAHASH(sura, aya)) {
	case AYAHASH(7, 206):
		return SAJDA_RECOM;
	case AYAHASH(13, 15):
		return SAJDA_RECOM;
	case AYAHASH(16, 50):
		return SAJDA_RECOM;
	case AYAHASH(17, 109):
		return SAJDA_RECOM;
	case AYAHASH(19, 58):
		return SAJDA_RECOM;
	case AYAHASH(22, 18):
		return SAJDA_RECOM;
	case AYAHASH(22, 77):
		return SAJDA_RECOM;
	case AYAHASH(25, 60):
		return SAJDA_RECOM;
	case AYAHASH(27, 26):
		return SAJDA_RECOM;
	case AYAHASH(32, 15):
		return SAJDA_OBLIG;
	case AYAHASH(38, 24):
		return SAJDA_RECOM;
	case AYAHASH(41, 38):
		return SAJDA_OBLIG;
	case AYAHASH(53, 62):
		return SAJDA_OBLIG;
	case AYAHASH(84, 21):
		return SAJDA_RECOM;
	case AYAHASH(96, 19):
		return SAJDA_OBLIG;
	default:
		return SAJDA_NONE;
	}
}
